import * as readline from "readline"
import { core } from "@enitoni/gears"

export class Adapter extends core.ClientAdapter<string, readline.Interface, {}> {
  protected register(_: any, hooks: core.AdapterHooks<string>) {
    const client = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })

    client.on("line", line => {
      hooks.message(line.trim())
    })

    return {
      client,
      methods: {
        start: async () => {},
        getMessageContent: (message: string) => message
      }
    }
  }
}

export type Matcher<D extends object = {}> = core.Matcher<D, string, readline.Interface>
export type Middleware<D = unknown> = core.Middleware<D, string, readline.Interface>
export type Context<D = unknown> = core.Context<D, string, readline.Interface>

export declare class Command<D = unknown> extends core.Command<
  string,
  readline.Interface,
  D
> {}

export declare class CommandGroup<D = unknown> extends core.CommandGroup<
  string,
  readline.Interface,
  D
> {}

export declare class Service extends core.Service<string, readline.Interface> {}
export declare class Bot extends core.Bot<string, readline.Interface> {}

exports.Command = core.Command
exports.CommandGroup = core.CommandGroup
exports.Service = core.Service
exports.Bot = core.Bot
