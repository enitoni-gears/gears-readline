# gears-readline

This package adds [readline](https://nodejs.org/api/readline.html) bindings to [Gears](https://www.npmjs.com/package/@enitoni/gears).

## Usage

```ts
import { Adapter, Bot } from "@enitoni/gears-readline"

const adapter = new Adapter({})
const bot = new Bot({ adapter, ... })

bot.start().then(() => {
  // bot is running, log something here if you want
})
```
